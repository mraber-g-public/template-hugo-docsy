module gitlab.com/mraber-g-public/template-hugo-docsy

go 1.22

require (
	github.com/google/docsy v0.11.0 // indirect
	github.com/joshed-io/reveal-hugo v0.0.0-20241030080325-e191f51d09be // indirect
)
