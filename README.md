# Documentation

## Introduction

This repository contains an example of a website for technical documentation.
It's published as a static website created with [Hugo] and the [Docsy] theme.

The site has to main sections:

* One about the product
  * This section should be made available on a public website after review
* One about the team itself

The documentation is published automatically on GitLab Pages.

The website hosts the [stable] version and a [preview] of each branches.

## Usage

### Prerequisites

If you plan to render the website locally, you have to install the following:

* [Hugo]
* [NPM]
* [Go]

> If you only intend to edit the documentation in Markdown format, you only need
> a text editor.

```bash
brew install go node hugo
npm install
```

### Test locally

You can view the website locally using the embedded server:

```bash
npm run clean ; npm run serve
```

The local server has an 'auto-reload' function, so you don't have to restart
the server after each modification.

### Deploy

Simply push your changes to the remote repository and wait for the pipeline to
run. After, you could get the URL using the 'Environment' section.
You can also view a list of all the branches [here][preview].

### Clean the cache

When working on a branch, a preview of the site is [published][preview].

When you are done, you should *stop* the associated *Environment* to clean the
cache and ensure the preview is unpublished when the next pipeline runs.

#### Limitation

As GitLab does not allow [publishing multiple branches on the same Pages][gitlab_issue]
for the moment, the pipeline implements a workaround using a shared cache between branches.
But it has some drawbacks.

First we have to configure a shared cache between protected and non protected branches
(see. *"Settings/CICD/General"*).

But most importantly in case of concurrent builds, due to how the cache is implemented,
some previews may be lost.

There is only one key in the cache for all the concurrent builds. So if a build
starts while another one is executing, both will start with the same cache content
as input and each build will produce a new cache entry only enriched with its own
production. So the last to write "wins".
The next build will not get from the cache the content of those last two builds.
It will only publish the preview of the one that ended last.

[docsy]:        https://www.docsy.dev/
[go]:           https://go.dev
[hugo]:         https://gohugo.io/
[npm]:          https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
[preview]:      https://mraber-g-public.gitlab.io/template-hugo-docsy/preview/
[stable]:       https://mraber-g-public.gitlab.io/template-hugo-docsy/
[gitlab_issue]: https://gitlab.com/groups/gitlab-org/-/epics/10914
