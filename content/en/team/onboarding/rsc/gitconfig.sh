cat <<'_EOF' >> $HOME/.gitconfig.macos
[core]
    pager = more -R

_EOF

cat <<'_EOF' >> $HOME/.gitconfig.home
[user]
    email = login@private-email.gitlab.com
_EOF

cat <<'_EOF' >> $HOME/.gitconfig.work
[user]
    email = login@mycompany.com
    #signingKey = 

[commit]
    #gpgSign = true

[gpg]
    #format = ssh

[gpg "ssh"]
    # Note: echo "$(git config --get user.email) namespaces=\"git\" $(cat <MY_KEY>.pub)" >> ~/sources/work-projects/allowed-signers-xxx
    #allowedSignersFile = ~/sources/work-projects/allowed-signers-xxx

[core]
    hooksPath = 
_EOF

cat <<'_EOF' >> $HOME/.gitconfig
[user]
    email = foo@bar.com
    name = FirstName LastName

[core]
    autocrlf = input
    editor = vim
    eol = lf
    fscache = true
    hooksPath = 

# requires git 2.28+
[init]
    defaultBranch = main

[color]
    ui = true

[status]
    submoduleSummary = true

[fetch]
    recurseSubmodules = true

[push]
    recurseSubmodules = check
    default = simple

[pull]
    rebase = true

[commit]
    gpgSign = false

[log]
    abbrevcommit = true

[alias]
    alias = config --get-regexp alias*
    br = branch
    changes = diff --name-status
    ci = commit
    cl = clone
    co = checkout
    cp = cherry-pick
    cpx = cherry-pick -xn
    dc = diff --cached
    fat = fetch --all --prune --tags --prune-tags
    last = log -1 HEAD
    # Afficher les changements réalisés depuis le dernier pull
    lc = !git oneline ORIG_HEAD.. --stat --no-merges
    lg = log --all --decorate --graph --oneline
    lgg = log --all --graph --date=relative --abbrev-commit --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %Cblue<%an>%Creset'
    oneline = log --pretty=oneline --abbrev-commit --graph
    pff = pull --ff-only --recurse-submodules=yes
    unstage = reset HEAD --
    st = status -sb
    start = !git init && git checkout -b main &&  git commit --no-verify --allow-empty -m \"Initial commit\"
    who = shortlog -sne
    orphans = branch --list --format "%(if:equals=[disparue])%(upstream:track)%(then)%(refname:short)%(end)"
    amend = commit --amend
    branch-name = !git rev-parse --abbrev-ref HEAD
    dic = diff --cached
    diffstat = diff --stat
    fix = commit --amend
    lg1 = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(black)%s%C(reset) %C(dim black)- %an%C(reset)%C(bold red)%d%C(reset)' --all
    lg2 = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold blue)%d%C(reset)%n''          %C(blue)%s%C(reset) %C(dim black)- %an%C(reset)' --all
    staged = diff --cached
    wdiff = diff --ignore-space-change

[gc]
    autoDetach = false

[diff]
    tool = bc3
    submodule = short
    color = always
    wserrorhighlight = all

[difftool "bc3"]
    trustExitCode = true

[merge]
    tool = bc3
    trustExitCode = true

[difftool]
    prompt = false

[gui]
    pruneduringfetch = true

[smartgit "submodule"]
    fetchalways = true
    update = true
    initializenew = true

#[http "https://website"]
#    proxy = <server>:<port>

#[credential "https://website"]
#    helper = cache --timeout 36000

[include]
    path = ~/.gitconfig.macos

[includeIf "gitdir:~/sources/work-projects/"]
    path = ~/.gitconfig.work

[includeIf "gitdir:~/sources/home-projects/"]
    path = ~/.gitconfig.home

_EOF
