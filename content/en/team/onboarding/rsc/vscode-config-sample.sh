cat <<'_EOF' >> "$HOME/Library/Application\ Support/Code/User/settings.json"
{
  "debug.console.fontSize": 12,
  "diffEditor.hideUnchangedRegions.enabled": true,
  "diffEditor.ignoreTrimWhitespace": false,
  "editor.accessibilitySupport": "off",
  "editor.bracketPairColorization.independentColorPoolPerBracketType": true,
  "editor.foldingImportsByDefault": true,
  "editor.fontFamily": "'JetBrains Mono Thin', 'Fira Code Light', 'Courier New', Menlo",
  "editor.fontLigatures": true,
  "editor.fontSize": 12,
  "editor.fontWeight": "normal",
  "editor.formatOnPaste": true,
  "editor.formatOnSave": true,
  "editor.guides.bracketPairs": true,
  "editor.linkedEditing": true,
  "editor.minimap.enabled": false,
  "editor.mouseWheelZoom": true,
  "editor.rulers": [80, 120],
  "editor.screenReaderAnnounceInlineSuggestion": false,
  "editor.stickyScroll.enabled": true,
  "editor.suggest.preview": true,
  "editor.tabCompletion": "onlySnippets",
  "editor.unicodeHighlight.nonBasicASCII": false,
  "files.autoGuessEncoding": true,
  "files.eol": "\n",
  "files.insertFinalNewline": true,
  "files.readonlyFromPermissions": true,
  "files.trimFinalNewlines": true,
  "gitlab.aiAssistedCodeSuggestions.enabled": false,
  "gitlab.duoChat.enabled": false,
  "gitlab.showPipelineUpdateNotifications": true,
  "gitlens.graph.dimMergeCommits": true,
  "go.editorContextMenuCommands": {
    "testFile": true
  },
  "go.inlayHints.assignVariableTypes": true,
  "go.lintTool": "golangci-lint",
  "go.toolsManagement.autoUpdate": true,
  "hediet.vscode-drawio.resizeImages": null,
  "hediet.vscode-drawio.theme": "atlas",
  "redhat.telemetry.enabled": false,
  "remote.autoForwardPorts": false,
  "scm.inputMinLineCount": 4,
  "[scminput]": {
    "editor.wordWrap": "off",
    "editor.rulers": [72, 80]
  },
  "telemetry.telemetryLevel": "error",
  "terminal.integrated.allowChords": false,
  "terminal.integrated.defaultProfile.osx": "zsh",
  "terminal.integrated.drawBoldTextInBrightColors": false,
  "terminal.integrated.fontFamily": "MesloLGS NF",
  "terminal.integrated.mouseWheelZoom": true,
  "terminal.integrated.scrollback": 5000,
  "window.confirmBeforeClose": "keyboardOnly",
  "window.zoomLevel": -0.75,
  "window.zoomPerWindow": true,
  "workbench.colorTheme": "Solarized Light",
  "workbench.commandPalette.history": 20,
  "workbench.editor.highlightModifiedTabs": true,
  "workbench.editor.splitInGroupLayout": "vertical",
  "workbench.iconTheme": "material-icon-theme",
  "workbench.tree.enableStickyScroll": true,
  "[json]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[jsonc]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[markdown]": {
    "editor.defaultFormatter": "yzhang.markdown-all-in-one",
    "editor.wordWrap": "off"
  },
  "[yaml]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  }
}
_EOF
