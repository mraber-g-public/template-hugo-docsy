cat <<'_EOF' >> $HOME/.vimrc
" vim: ft=vim

" ===== GENERAL
" ================================

" Undo limits
set history=500

" filetype plugin
filetype plugin on
filetype indent on

" Save file with 'sudo'
command! W execute 'w !sudo tee % >/dev/null' <bar> edit!

" ===== VISUAL
" ================================
" Set color scheme (dark terminal)
"colorscheme ThemerLucid
colorscheme industry
"colorscheme peachpuff

" Set the window's title to the file being edited
set title

" Show line numbers on the sidebar
set number

" Always show cursor position
set ruler

" Display command line's tab complete options as a menu
set wildmenu

" Highlight the current line
set cursorline

" Enable Modelines
set modeline
set modelines=5

" Enable syntax highlighting
syntax on

" Show matching brackets
set showmatch
" how many tenths of a second to blink
set mat=2

set noerrorbells
set novisualbell

" ===== Text, indent, tabs
" ================================
" Unix as default file type
set ffs=unix,dos,mac

" Replace tabs with spaces
set expandtab

" tab size (display)
set tabstop=4
" tab size (editing)
set softtabstop=4
" indentation size
set shiftwidth=4

" Insert 'tabstop' number of spaces when 'tab' is pressed
set smarttab

" Better handling of 'backspace'
set backspace=indent,eol,start

" copy indendation of previous line
set autoindent

" wraps lines: no
set nowrap

" hard wrap
set textwidth=100

" ===== Search
" ================================
" case sensitive if at least one upercase
set smartcase
set hlsearch

" ===== Backups
" ================================
set nobackup
set nowb
set noswapfile

"set spell
" TODO

" ===== Editing: YAML & JSON
" ================================
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType json setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType markdown setlocal tw=120

" ===== Editing: Shell Script
" ================================
autocmd FileType sh setlocal ts=2 sts=2 sw=2 expandtab

_EOF
