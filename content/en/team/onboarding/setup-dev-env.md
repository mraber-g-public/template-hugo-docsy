---
title: Setup Development Environment
linkTitle: Setup Dev Env
weight: 30
description: How to configure your new laptop.
icon: fa-solid fa-screwdriver-wrench
---

{{% pageinfo %}}
This section provides a new member with some advices on how to configure its
laptop.
{{% /pageinfo %}}

## Prerequisites

* Have a machine
* Have an access to GitLab

## Self-Service

Use the "Self-Service" app to install:

* Brew (on Mac)
* Draw.io
* Figma
* Google Chrome
* iTerm2 (on Mac)
* KeePass XC (on Mac)
* Microsoft Outlook
* Microsoft Teams (choose "new Teams" once installed)
* VSCode

{{% info %}}
Other software and tools will be installed later on using `brew` (Mac)
{{% /info %}}

### Create an SSH key

This key will be used to access GIT repositories on GitLab.

{{% danger %}}
This key shall be password-protected (use KeePass to store it if you want).
{{% /danger %}}

```bash
NAME=<yourname>
mkdir $HOME/ssh-keys
ssh-keygen -t ed25519 -C "${NAME}@gitlab-foobar" -f $HOME/ssh-keys/${NAME}-laptop-gitlab-foobar
cat $HOME/ssh-keys/${NAME}-laptop-gitlab-foobar.pub
```

{{% info %}}
Use `ssh-agent` to avoid having to enter the password all the time.
{{% /info %}}

#### Manage multiple keys

If you want to use multiple SSH keys, you can configure you SSH client by editing
the `~/.ssh/config` file.

Here is an example to use two keys on two different hosts:

* a first key located in `$HOME/ssh-keys/work/gitlab-pro` for `gitlab.awesome-company.io`
* a second one located at `$HOME/ssh-keys/home/gitlab` for `gitlab.com`.

```sh
# cat ~/.ssh/config

Host gitlab.awesome-company.io
  PreferredAuthentications publickey
  IdentityFile ~/ssh-keys/work/gitlab-pro

# GitLab.com
Host gitlab.com
  PreferredAuthentications publickey
  IdentityFile ~/ssh-keys/home/gitlab
```

### Misc: Fonts

Some fonts, you may find interesting:

* [JetBrains Mono](https://www.jetbrains.com/lp/mono/)
* [Meslo LGS NF](https://github.com/romkatv/powerlevel10k/blob/master/font.md)
  (useful for `p10k`)
* [Intel One Mono](https://github.com/intel/intel-one-mono)

## Additional Software

### On OSX, with `brew`

{{% info %}}
You may need 'admin' privileges.
{{% /info %}}

```bash
# Install a recent Bash version
brew install bash
brew install bats-core
brew install git
brew install htop
brew install cowsay
brew install watch

brew install jq
brew install yq
brew install yamllint
brew install markdownlint-cli

brew install ncdu
brew install tmux

brew install npm

brew install podman

brew install derailed/k9s/k9s
brew install kubernetes-cli
brew install helm
brew install kustomize

brew install sops age
brew install cosign

brew install fluxcd/tap/flux

brew install azure-cli
brew install Azure/kubelogin/kubelogin

brew install glab
glab config set --global host gitlab.awesome-company.io

# OPTIONAL:
# OpenJDK by Eclipse
brew install --cask temurin

# Licensed software
brew install --cask beyond-compare
brew install --cask smartgit

# OPTIONS:
brew install weaveworks/tap/gitops
```

### `kubectl` plugin management

Install `krew`:

```bash
(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
  tar zxvf "${KREW}.tar.gz" &&
  ./"${KREW}" install krew
)
echo 'export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"' >> "$HOME/.zshrc"

# Krew: plugins
kubectl krew install access-matrix
kubectl krew install hns
```

## Configuration

### VSCode

#### Mandatory extensions

```bash
echo "VSCode: installing extensions..." \
  && code --install-extension chrislajoie.vscode-modelines \
  && code --install-extension davidanson.vscode-markdownlint \
  && code --install-extension esbenp.prettier-vscode \
  && code --install-extension redhat.vscode-xml \
  && code --install-extension redhat.vscode-yaml \
  && code --install-extension timonwong.shellcheck \
  && echo "VSCode: extensions installed."
```

#### Useful extensions

```bash
echo "VSCode: installing extensions..." \
  && code --install-extension adpyke.codesnap \
  && code --install-extension alefragnani.bookmarks \
  && code --install-extension bierner.markdown-mermaid \
  && code --install-extension chrmarti.regex \
  && code --install-extension eamodio.gitlens \
  && code --install-extension gitlab.gitlab-workflow \
  && code --install-extension hashicorp.terraform \
  && code --install-extension hediet.vscode-drawio \
  && code --install-extension ms-azuretools.vscode-azureterraform \
  && code --install-extension ms-azuretools.vscode-docker \
  && code --install-extension ms-kubernetes-tools.vscode-kubernetes-tools \
  && code --install-extension ms-python.debugpy \
  && code --install-extension ms-python.python \
  && code --install-extension ms-python.vscode-pylance \
  && code --install-extension ms-vscode-remote.remote-containers \
  && code --install-extension ms-vscode-remote.remote-ssh \
  && code --install-extension ms-vscode-remote.remote-ssh-edit \
  && code --install-extension ms-vscode-remote.remote-wsl \
  && code --install-extension ms-vscode-remote.vscode-remote-extensionpack \
  && code --install-extension ms-vscode.azure-account \
  && code --install-extension ms-vscode.remote-explorer \
  && code --install-extension ms-vscode.remote-server \
  && code --install-extension pnp.polacode \
  && code --install-extension rangav.vscode-thunder-client \
  && code --install-extension seatonjiang.gitmoji-vscode \
  && code --install-extension stkb.rewrap \
  && code --install-extension weaveworks.vscode-gitops-tools \
  && code --install-extension yzhang.markdown-all-in-one \
  && echo "VSCode: extensions installed."
```

#### Useful Themes

```bash
echo "VSCode: installing themes..." \
  && code --install-extension dracula-theme.theme-dracula \
  && code --install-extension equinusocio.vsc-material-theme \
  && code --install-extension equinusocio.vsc-material-theme-icons \
  && code --install-extension github.github-vscode-theme \
  && code --install-extension pkief.material-icon-theme \
  && code --install-extension wesbos.theme-cobalt2 \
  && echo "VSCode: themes installed."
```

#### Configuration

{{< readfile file="rsc/vscode-config-sample.sh" code="true" lang="bash" >}}

### Git

{{< readfile file="rsc/gitconfig.sh" code="true" lang="bash" >}}

### Vim

{{< readfile file="rsc/vimrc.sh" code="true" lang="bash" >}}

### Shell: `ssh-agent`

To start `ssh-agent` automatically, add the following configuration to `$HOME/.zshrc` (or equivalent):

```bash
if [[ -e $HOME/.sshagent.conf ]]; then
        . $HOME/.sshagent.conf >/dev/null
fi
if $(ps -p ${SSH_AGENT_PID}>/dev/null 2>&1);then true;
else
        ssh-agent >| $HOME/.sshagent.conf
        . $HOME/.sshagent.conf >/dev/null
fi
[[ -e $HOME/.sshagent.conf ]] && chmod 600 $HOME/.sshagent.conf
```

### tmux with Powerline

```bash
# cat ~/.tmux.conf
set -g default-terminal "screen-256color"
set -g status-interval 2

source /opt/homebrew/lib/python3.11/site-packages/powerline/bindings/tmux/powerline.conf
```
