---
title: Onboarding
linkTitle: Welcome
weight: 10
description: Onboard a new member in the team.
icon: fa-solid fa-rocket
---
