---
title: Meeting about Foo
date: 2006-01-02
linkTitle: Foo
description: >
    Summary of the meeting.
---

{{% titleblock %}}
<!-- markdownlint-disable MD032 MD007 -->
Date
: 2006-01-02 (15h - 16h)

Participants
: * Jane Doe
  * John Smith
  * Bob Sunlight
<!-- markdownlint-enable MD032 MD007 -->
{{% /titleblock %}}

## Notes

TODO
