---
title: Team's Internal Documentation
linkTitle: Team
menu: {main: {weight: 30}}
cascade:
  params:
    type: "docs"
    toc_root: false
---

## Mission

TODO

## Members

{{% cardpane %}}
<!-- markdownlint-disable MD013 -->

{{% card header="[**Jane Doe**](https://gitlab.com/janedoe999)"
    title="Product Owner"
    subtitle=""
    footer="[![avatar](https://api.dicebear.com/8.x/croodles/svg?size=64&seed=janedoe)](https://gitlab.com/janedoe999)" %}}

[Email][email_jdoe] - [Teams][teams_jdoe]

[email_jdoe]: mailto:jane.doe@awesome-company.dev
[teams_jdoe]: https://teams.microsoft.com/l/chat/0/0?users=jane.doe@awesome-company.dev
{{% /card %}}

{{% card header="[**Bob Sunlight**](https://gitlab.com/foobar)"
    title="Tech Lead"
    subtitle="Awesome Guy!"
    footer="[![avatar](https://api.dicebear.com/8.x/croodles/svg?size=64&seed=bobsunlight)](https://gitlab.com/janedoe999)" %}}

[Email][email_bl] - [Teams][teams_bl]

[email_bl]: mailto:bob@awesome-company.dev
[teams_bl]: https://teams.microsoft.com/l/chat/0/0?users=bob@awesome-company.dev
{{% /card %}}

{{% /cardpane %}}

{{% cardpane %}}

{{% card header="[**John Smith**](https://gitlab.com/foobar)"
    title="Developer"
    subtitle="Awesome Guy!"
    footer="[![avatar](https://api.dicebear.com/8.x/croodles/svg?size=64&seed=johnsmith)](https://gitlab.com/janedoe999)" %}}

[Email][email_john] - [Teams][teams_john]

[email_john]: mailto:john@awesome-company.dev
[teams_john]: https://teams.microsoft.com/l/chat/0/0?users=john@awesome-company.dev
{{% /card %}}

<!-- markdownlint-enable MD013 -->
{{% /cardpane %}}

{{% info %}}
To reach the entire team, use the [mailing-list](mailto:awesome-team@awesome-company.dev)
{{% /info %}}

## Rituals

TODO (Describe how the teams works in a few words)

* Daily Meeting
* [Cycle (release)](./team-workflow/time-mgmt/)
* [Retrospective](./team-workflow/retrospectives/)

## Communication

TODO (how do we communicate with each others, in the teams and in the company)

<!-- LINKS -->
