---
title: Retrospectives
linkTitle: Retro
weight: 30
description: Minutes of the team retrospectives.
---

_A retrospective is a standard meeting in agile organizations where the team
reflects on the past to improve the future._

The team meets every 3 weeks on Wednesday for 90 minutes to discuss about what
happened during the last period and to identify:

- the mood of the team's members
- what went well
- what can be improved
- what must be changed / stopped
- etc.
