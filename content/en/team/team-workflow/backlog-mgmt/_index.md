---
title: Backlog Management
linkTitle: Backlog
weight: 20
description: How our backlog is managed.
---

## Workflow

As described in [time management](../time-mgmt), the team works in 2-week sprints.

## Labels

### Planning

To (_quickly_) estimate our work, we use the labels **`estim:XS`, `estim:S`,
`estim:M`, `estim:L`, `estim:XL`, `estim:XXL`**.

For further information, see ["Estimation"](../time-mgmt/#estimation)

##### Spike

Spike are for exploration of a subject we do not know before hand. As such,
we can't evaluate their complexity. Instead we allocate some time to work
on it (upper-bound).

#### Priority

To prioritize our work, we use the labels **`P::1`, `P::2`, `P::3`, `P::4`** with the
following definition:

{{% example %}}

* _Priority 1_: Urgent. Someone should address this as soons as possible.
* _Priority 2_: High. This should be addressed during the next 3 months (_release_)
* _Priority 3_: Medium. This should be addressed during the next 6 months
* _Priority 4_: Low. No timeline for the moment

{{% /example %}}

### Progress

To [track progress][board_wip], we use the labels: **`status:backlog`,
`status:dependencies`, `status:ready`, `status:dev`, `status:to-be-reviewed`,
`status:reviewing`, `status:reviewed`, `status:released`**.

{{% titleblock %}}

_Open_
: Default state of an issue after its creation.
  Needs triage.

status:backlog
: Issue 'accepted' by the team after 'triage' but it needs to be refined
  (see [below](#open-vs-backlog) for further explanations).

status:dependencies
: Issue is blocked by an _external_ event.

status:ready
: Issue is ready to be taken by a team member.
  [DoR](#definition-of-ready-1) shall be satisfied.

status:dev
: Actively working on the issue

status:to-be-reviewed
: Issue is complete. [DoD](#definition-of-done-1) is validated.
  MR is no more in 'draft' and could be reviewed by others.

status:reviewing
: MR is actively reviewed by one or more persons.
  For major remarks, the associated issue shall return to `status:dev`.

status:reviewed
: MR is accepted by all reviewers and ready _to be merged_ if acceptance tests pass.

_status:released_
: Only used for deployment to 'production'. Usage must be clarified.

_Closed_
: MR is merged or action is done.

{{% /titleblock %}}

#### Open vs Backlog

The difference between the default status of _Open_ and the label _Status::Backlog_
may not seem obvious at first sight.

First a ticket is created and only has the status of _Open_. Here it will be qualified
and challenged by the product owner (mainly).

Once the ticket has been qualified as valid and has enough information, it's
flagged with `status::backlog`. At this stage, the ticket can be reviewed by the
team (during a _refinement_ meeting for example): missing info, add estimation,
add acceptance tests, _etc._.

Finally, when all the [DoR](#definition-of-ready-1) criteria are met, the ticket
will be flagged with `status::ready` and it will be added to an iteration.

#### Work In Progress limits

To help detect problems in our flow, the following WIP limits are configured
on the daily board:

|         Status          | Limit |                                 Comment                                 |
| :---------------------: | :---: | :---------------------------------------------------------------------: |
|    `status:backlog`     |   1   | _(An issue not ready shouldn't have been put in the current iteration)_ |
|  `status:dependencies`  |   2   |            _(It may indicate that the iteration is at risk)_            |
|      `status:dev`       |   6   | _(Work in progress should be roughly equals to the size of the squad)_  |
| `status:to-be-reviewed` |   3   |                                                                         |
|   `status:reviewing`    |   4   |                                                                         |

### Others

The following labels may also be used, depending on the situation:

* **`triage - needs-more-info`**: Issue lacks a description or it's not sufficient.
* **`triage - duplicate`**: Issues that are duplicates of other existing issues
* **`continuous improvement`**: actions linked with continuous improvement
* **`spike`**: Issue for investigating an implementation or learning.
               It will be measured in time and not story points (w).
* **`unplanned`**: Issue add to the current release after the scoping


## Agile Items

### Milestone

The GitLab object "_milestone_" materialized a 3-month release cycle (5 sprints).

The start date is the first Monday of the first sprint.

The end date is the first Monday following the 5th sprint (see [iteration](#iterations)
below for the rationale).

{{% warning %}}
Only issues can be associated with a milestone so at the epic level, a label is
used (see below).
{{% /warning %}}

### Iterations

The GitLab object "_iteration_" materialized a 2-week sprint.

As the team holds its "sprint planning" meeting every other Monday (afternoon),
the iteration:

* starts the Tuesday morning
* ends the Monday of the next sprint planning.

### Epic

#### Definition

An epic describes a feature to deliver during the release. Multiple _stories_ are
generally required to implement it.

An epic shouldn't span over multiple releases (milestone).

#### Metadata

Several label are used to organized the epics:

* Set the `"Cycle NN"` (where `NN` is a number) label to link the epic to one or more milestones

#### Definition of Ready

TODO

#### Definition of Done

TODO

### Story

#### Definition

TODO

#### Metadata

The following metadata shall be provided on each issue in addition to the description:

At creation:

* Epic
* Labels:
  * `"estim::undef"` label

At creation or during scoping / planning:

* Milestone
  * should be the same as the epic
* Iteration
* Weight

#### Definition of Ready

Whether it's functional or technical, the team agreed upon the following
criteria:

1. Clear description and definition
    * each member of the team has a clear understanding of the expected output
1. An estimation is available ([method](../time-mgmt#estimation))
1. Acceptance criteria are written (_test cases_)

#### Definition Of Done

Whether it's functional or technical, the team agreed upon the following
criteria:

1. Acceptance criteria are satisfied
1. NRG pipeline pass
1. Unit tests (when applicable) have been written
1. Peer reviewed
1. Documentation updated (both technical and for final users)
1. SAST successful
1. Conventions & styles are respected

<!-- LINKS -->
[board_wip]: https://gitlab.com/
