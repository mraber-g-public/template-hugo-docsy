---
title: Time's Management
linkTitle: Time
weight: 10
description: How we are cadenced and how we estimate the work to do.
---

## Organization

### Project level

Our project, and so the different teams, is organized in 3-month releases,
themselves divided into 2-week sprints.

Each release (called _"cycle"_) is broken down as follows:

```mermaid
timeline
  title Release
  section Plan
    Scoping : week 2
  section Execute
    Sprint 1 : week 3 : week 4
    Sprint 2 : week 5 : week 6
    Sprint 3 : week 7 : week 8
    Sprint 4 : week 9 : week 10
    Sprint 5 : week 11 : week 12
```

- 1st week is the _Scoping week_:
  - the CPO and POs present the objectives of the release (_Epics_) to all the teams
  - Each team builds its backlog for the cycle and identifies dependencies
    with the other teams
- 2-week _sprints_

{{% info %}}
All epics and releases are available in [GitLab][board_epics].
{{% /info %}}

### Team level

To better plan our work, give visibility and detect any drift or risk on the
release, we choose to materialize our sprints using the concept of
_iteration_ provided by GitLab.

In GitLab the main useful boards are:

- [Iterations' content][board_iter]
- [Work In Progress][board_wip]
- [Current Sprint][board_daily]

## Capacity

During the scoping, the capacity of the team must be calculated so that it can
commit to a result.

The capacity is evaluated as such: `Wd x Cr x Vm` with:

- `Wd`: Working days for this cycle
- `Cr`: Cost of the different rituals (see below)
- `Vm`: Average velocity based on the previous releases

The capacity is in an arbitrary unit called _Story Point (SP)_. More on that below.

{{% warning %}}
This method of evaluation is a work in progress.
{{% /warning %}}

### Cost of agile rituals

A percentage of `40%` of the raw quantity of working days is reserved for the
different rituals (review, retrospective, daily, etc.).

{{% info %}}
In theory this amount shouldn't be necessary and be incorporated in the
average velocity.
{{% /info %}}

### Estimation

The team evaluates the tasks using "t-shirt sizes" (see below for the ratio).
The main advantages are:

- efficiency (a lot of tasks can be evaluated quickly during the scoping)
- fairly accurate

The following table describes the available size with their ratio and the
corresponding _weight_ as defined by GitLab:

| Size  | Ratio | Weight |     Comment     |
| :---: | :---: | :----: | :-------------: |
| `XS`  |   0   |   0    |    task < 2h    |
|  `S`  |   1   |   1    |                 |
|  `M`  |  3S   |   3    |                 |
|  `L`  |  2M   |   6    |                 |
| `XL`  |  4M   |   12   | should be split |
| `XXL` |  2XL  |   24   | shall be split  |

Visually it looks like:

```text
+---+
| S |
+---+---+---+
|     M     |
+---+---+---+---+---+---+
|           L           |
+---+---+---+---+---+---+---+---+---+---+---+---+
|                       XL                      |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|                                              XXL                                              |
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
```

{{% info %}}
These ratio are purely arbitrary and can be re-evaluated by the team if the
need arises.

Though they should be used long enough for the team to get insights and be
capable of calculated its velocity.
{{% /info %}}

#### How-to estimate

If a lot of _Stories_ have to be evaluated (for example during the scoping),
the following method is used:

- Open the ["Poker" board in GitLab][board_poker]
- Add filters to match the current cycle / sprint / etc.
- Drag & Drop each card onto the _right_ column

![Poker Planning in GitLab](poker.png)

{{% info %}}
After that, you still have to fill in the _weight_ manually (see "_bulk edit_").
{{% /info %}}

<!-- LINKS -->
[board_iter]:  https://gitlab.awesome-company.io/groups/mygroups/-/boards/0000
[board_wip]:   https://gitlab.awesome-company.io/groups/mygroups/-/boards/0000
[board_daily]: https://gitlab.awesome-company.io/groups/mygroups/-/boards/0000
[board_poker]: https://gitlab.awesome-company.io/groups/mygroups/-/boards/0000
[board_epics]: https://gitlab.awesome-company.io/groups/mygroups/-/boards/0000
