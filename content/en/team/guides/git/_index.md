---
title: Source Code Management
linkTitle: GIT
#weight: 10
description: How to use GIT
icon: fa-solid fa-code-compare
---

This section documents the team guidelines regarding source code management
with [GIT] and [GitLab].

## Commit message

A good commit message is important as it will communicate the context about the
change to all the people who will have to review and maintain the code.

![Commit message, XKCD](git_commit.png)

A commit message is composed of a _subject_ and a _body_ separated by an
empty (blank) line.

{{< columns >}}

The **_subject_** line:

* uses the imperative mood
  * (_[If applied this commit will]..._)
* Describes the _intent_ of the commit
* is capitalized
* is limited to 50 characters or so
* doesn't end with a period.

<--->

The **_body_**:

* explains the _what_ and _why_, not the _how_
* is limited to 72 characters or so.
* may contain paragraphs, lists of items, etc.

{{< /columns >}}

{{% info %}}
The _how_ is described by the code itself. Though, the commit message may
contain explanations about the implementation if not obvious.
{{% /info %}}

### Examples

Below some examples of "real world" commit messages following those conventions.

#### Example 1

{{< card header="**Don't**" code=true lang="text" >}}
Fix an error in fooctl
{{< /card >}}

{{< card header="**Do**" code=true lang="text" >}}
Fix fooctl random error when computing version SHA

Recent versions of fooctl check if the binary is up-to-date with the
source code. To do that, we compute a SHA of the source code and
compare it with the one embedded in the binary at build time.

SHA computation is based on the output of the 'find' command but the
order of the latter isn't guaranteed (FS dependent).

So, we add a sorting step before computing the SHA.
{{< /card >}}

#### Example 2

{{< card header="**Don't**" code=true lang="text" >}}
Fix NRG
{{< /card >}}

{{< card header="**Do**" code=true lang="text" >}}
Change separator in the binary version metadata section

When building temporary binaries for NRG testing, Helm charts can't be
packaged if the commit SHA starts with a zero ('0').

Helm complains that 'Error: validation: chart.metadata.version
"0.18.0-PR5068.367--mode.opt.sha.0328961.state.snapshot.dirty" is
invalid'.

Indeed Helm3 checks that the version field complies with the Semver
format (or at least looks like a Semver version; the control isn't
strict). In particular, the Go library used by Helm3 requires that the
prerelease number doesn't have leading zeros.

In addition, due to a limitation of Docker, we were forced to replace
the '+' delimiter required by Semver with the '--' delimiter.
Consequently, the metadata section isn't recognized as such by the
library but as a prerelease section.

So, all temporary version where the GIT SHA starts with a '0' are
invalid for Helm3.

The workaround is to use another valid character ('-') to separate key
and value in the metadata section.

Old format: 0.18.0-PR5068.367--mode.opt.sha.0328961.state.snapshot
New format: 0.18.0-PR5068.367--mode-opt.sha-0328961.state-snapshot
{{< /card >}}

#### Example 3

{{< card header="**Don't**" code=true lang="text" >}}
Set NodeLocal DNS cache version to 1.22.23
{{< /card >}}

{{< card header="**Do**" code=true lang="text" >}}
Fix NodeLocal DNS cache crashes on RHEL8

When deploying K8S (1.21.10) on RHEL 8, the Node Local DNS Cache
DaemonSet is in 'Crashloopback'.

The logs indicates that it can't create a rule in iptables' table 'raw'
because it doesn't exist. Besides, the logs show that it runs in
'legacy' mode (not netfilter) while the iptables on the host is in the
new 'netfilter' mode.

* Container: iptables v1.8.2 (legacy)
* Host: iptables v1.8.4 (netfilter)

Note: the current version works on our CentOS 8 hosts, but with a
slightly older kernel version (4.18.0-193 vs 4.18.0-348).

The solution is to update the version from 1.15.13 (May 2020) to the
latest available (1.22.23, July 2023).
{{< /card >}}

#### External examples

##### Go language

Example of a [recent commit][russ] from Russ Cox on the Golang source code
to fix a "bug" in the `Timer` management.

{{< card header="**Don't**" code=true lang="text">}}
Fix timer Reset
{{< /card >}}

{{< card header="**Do**" code=true lang="text">}}
time: avoid stale receives after Timer/Ticker Stop/Reset return

A proposal discussion in mid-2020 on #37196 decided to change
time.Timer and time.Ticker so that their Stop and Reset methods
guarantee that no old value (corresponding to the previous configuration
of the Timer or Ticker) will be received after the method returns.

The trivial way to do this is to make the Timer/Ticker channels
unbuffered, create a goroutine per Timer/Ticker feeding the channel,
and then coordinate with that goroutine during Stop/Reset.
Since Stop/Reset coordinate with the goroutine and the channel
is unbuffered, there is no possibility of a stale value being sent
after Stop/Reset returns.

Of course, we do not want an extra goroutine per Timer/Ticker,
but that's still a good semantic model: behave like the channels
are unbuffered and fed by a coordinating goroutine.

The actual implementation is more effort but behaves like the model.
Specifically, the timer channel has a 1-element buffer like it always has,
but len(t.C) and cap(t.C) are special-cased to return 0 anyway, so user
code cannot see what's in the buffer except with a receive.
Stop/Reset lock out any stale sends and then clear any pending send
from the buffer.

Some programs will change behavior. For example:

	package main

	import "time"

	func main() {
		t := time.NewTimer(2 * time.Second)
		time.Sleep(3 * time.Second)
		if t.Reset(2*time.Second) != false {
			panic("expected timer to have fired")
		}
		<-t.C
		<-t.C
	}

This program (from #11513) sleeps 3s after setting a 2s timer,
resets the timer, and expects Reset to return false: the Reset is too
late and the send has already occurred. It then expects to receive
two values: the one from before the Reset, and the one from after
the Reset.

With an unbuffered timer channel, it should be clear that no value
can be sent during the time.Sleep, so the time.Reset returns true,
indicating that the Reset stopped the timer from going off.
Then there is only one value to receive from t.C: the one from after the Reset.

In 2015, I used the above example as an argument against this change.

Note that a correct version of the program would be:

    func main() {
      t := time.NewTimer(2 * time.Second)
      time.Sleep(3 * time.Second)
      if !t.Reset(2*time.Second) {
        <-t.C
      }
      <-t.C
    }

This works with either semantics, by heeding t.Reset's result.
The change should not affect correct programs.

However, one way that the change would be visible is when programs
use len(t.C) (instead of a non-blocking receive) to poll whether the timer
has triggered already. We might legitimately worry about breaking such
programs.

In 2020, discussing #37196, Bryan Mills and I surveyed programs using
len on timer channels. These are exceedingly rare to start with; nearly all
the uses are buggy; and all the buggy programs would be fixed by the new
semantics. The details are at [1].

To further reduce the impact of this change, this CL adds a temporary
GODEBUG setting, which we didn't know about yet in 2015 and 2020.
Specifically, asynctimerchan=1 disables the change and is the default
for main programs in modules that use a Go version before 1.23.
We hope to be able to retire this setting after the minimum 2-year window.
Setting asynctimerchan=1 also disables the garbage collection change
from CL 568341, although users shouldn't need to know that since
it is not a semantically visible change (unless we have bugs!).

As an undocumented bonus that we do not officially support,
asynctimerchan=2 disables the channel buffer change but keeps
the garbage collection change. This may help while we are
shaking out bugs in either of them.

Fixes #37196.

[1] <https://github.com/golang/go/issues/37196#issuecomment-641698749>

Change-Id: I8925d3fb2b86b2ae87fd2acd055011cbf7bd5916
Reviewed-on: https://go-review.googlesource.com/c/go/+/568341
Reviewed-by: Austin Clements <austin@google.com>
Auto-Submit: Russ Cox <rsc@golang.org>
LUCI-TryBot-Result: Go LUCI <golang-scoped@luci-project-accounts.iam.gserviceaccount.com>

{{< /card >}}

[russ]: https://go-review.googlesource.com/c/go/+/568341

## Merge Review

Each change is integrated into the codebase through a _merge request_ that
should be validated by at least one peer (project dependent).

Some recommendations to follow for the reviewee:

* Avoid to 'force push' a branch once the MR is opened for review
  (not anymore with the "Draft" status)
* Only add commits and prefix them with `"[FIXUP]"` or `"[SQUASH]"` to clearly
  show the intent
  * those commits will be squashed one the PR is approved, before being merged
    into the target branch
  * it allows the reviewers to easily check the correction

Some recommendations to follow for the reviewers:

* Use "start a review" and create threads. Do _not_ simply add 'comments'
  * This way, we can follow the corrections and block the MR until all discussions
    are closed.
* You may prefix a comment to show its criticality:
  * `"[minor]"`, `"[major]"`, `"[blocker]"`
* Provides with a suggestion or an example of code if possible
* Avoid remarks that reflect personal preferences

{{% info %}}
In case of too many remarks, it may be easier to stop the review and plan a
meeting with the author.
{{% /info %}}

<!-- LINKS -->
[git]: https://www.git-scm.com
[gitlab]: https://about.gitlab.com/
