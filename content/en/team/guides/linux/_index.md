---
title: Shell Scripting
linkTitle: Scripts
#weight: 10
description: Shell script style guide
icon: fa-brands fa-linux
---

## Conventions

* Scripts shall be written for `bash` shell
  * Be careful with the version used and the available functionality
    (for example MacOS has a very old one with no associative array)
  * if a specific version is required:
    * it shall be documented
    * the script shall check for it
* Scripts shall be unit-tested (see `bats` framework)
* Scripts shall check for errors (exit code)

## Unit-testing Shell Script

{{% info %}}
Coming soon!
{{% /info %}}
