---
title: GitLab
linkTitle: GitLab
#weight: 10
description: GitLab Pipelines conventions and best practices.
icon: fa-brands fa-gitlab
---

## Conventions

{{% info %}}
_All the conventions described here aren't yet applied to all our pipelines as
the majority predate this document._
{{% /info %}}

### Naming conventions

TBD

## Scripts

* Be careful to not hide the error and the exit code (see 'Pitfalls' below)
* Every pipeline should define the image to use
  * This, to ensure the shell and tools behave correctly
    (e.g.: `sh` vs `bash`, lightweight `curl` in BusyBox)

## Common pitfalls

### Folded string block (`">"`)

* Used to split a long command on multiple lines
* GitLab will join all the lines to create a single command line
* Be careful to **not** indent the lines or else each one will be considered
  as a distinct command

### Literal string block (`"|"`)

* Used to provide with a sequence of commands
  
### Compound commands (`"cmd1 && cmd2"`)

* Always surround it with parenthesis else the exit code will always be `0` (success)
  * E.g: `- (cmd 1 && cmd2)` instead of `- cmd1 && cmd2`

### `":"` in command

* If a line contains the character `:` it must be surrounded with **single quotes** (`''`)

### `curl` command

You shall use the following options:

* `--fail`: exit with an error in case of an HTTP error (>=400)
  * `--fail-with-body` may be used instead to get the message returned by the server
* `--show-error`: display a message in case of errors when `--silent` is used

## Merge Request Settings

### Merge Request Method

#### Context and problem statement

The default merge method ("merge commit") creates a new commit object for each
fusion of a feature branch into a target branch. But as we only work with
merge request and that they are for a vast majority very small (often only one
commit), the GIT history is polluted with meaningless commit message (roughly
50%). The only useful information carried by that merge commit is the reference
to the merge request in GitLab.

Besides, the source branch of such a merge commit can have its origin everywhere
in the target branch. So, there is no linearity in the GIT history, which makes
it difficult to read and understand.

Hence, it may be difficult, for external users, to follow up with the changes
delivered.

#### Pros and cons of each solution

GitLab offers three different merge methods with each their pros and cons.

| **Merge Request Method**                  | **Pros**                                                     | **Cons**                                                                                    |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------------------------------------- |
| **Merge Commit**                          | - Clear record of merges.                                    | - Can create a cluttered commit history.                                                    |
|                                           | - Easy to understand and use.                                | - History can become less linear and harder to follow                                       |
|                                           | - Maintains the context of feature branch.                   |                                                                                             |
| **Fast-Forward Merge**                    | - Maintains a clean, linear history.                         | - No explicit record of merges, making it harder to track when branches were merged.        |
|                                           | - Simple and quick, no extra commits created.                | - Can hide the context of the feature branch.                                               |
|                                           | - Clearer commit messages                                    |                                                                                             |
| **Merge Commit with Semi-Linear History** | - Combines benefits of rebase and merge commits.             | - Potential for loss of context due to rebasing.                                            |
|                                           | - Results in a linear history with clear merge points.       | - Rewriting history can cause issues if multiple developers are working on the same branch. |
|                                           | - Avoids cluttered history while still showing merge events. |                                                                                             |
|                                           |                                                              |                                                                                             |

#### Chosen solution

Branches are now merged following the **merge commit with semi-linear history**
method to ensure a linear history of commits messages while still retaining the
context information (merge request id and so the related issue(s)).

It only requires that the source branch is up-to-date with the target branch,
which means regular rebasing by the developer (which is a good thing in any case).

{{% info %}}
We may migrate to _"fast-forward merge"_ later on to have an even better signal/noise
ratio in the GIT history (cutting in half the number of commits in the history).

But it requires that we provide with a solution to _automatically_ add a trailer
to each commit message (`Issue-Id: 1234`) to keep traceability.

Enforcing the trailer is quick & easy: configure a regex to validate the commit
message server side (see GitLab's project settings).
But setting it automatically client-side is more challenging as it requires to
activate "hooks" locally for each developer and to find a reliable way to get
the issue's id.
{{% /info %}}

### Merge Options

The following options have been set:

* **Enable merged results pipelines**:
  * The pipeline must succeed on the result of the merge of the two branches to
    ensure that the branch fusion doesn't introduce any bugs.
* **Show link to create or view an MR when pushing from the command line**:
  * For CLI users' comfort.
* **Enable "Delete source branch" option by default**:
  * This avoids keeping branches that we don't need anymore.

### Squash Commits When Merging

The checkbox is unselected by default because we believe it is the user's
responsibility to squash their commits into an atomic and understandable commit.

### Merge Checks

A merge request can be merged only if:

* **The pipelines are successful.**:
  * This avoids introducing failures in the target branch.
* **All threads are resolved.**:
  * Every remarks should be taken into account to avoid bugs and unresolved problems.

### Merge Request Approvals

The project should have a `CODEOWNERS` file and be configured to require approval
from code owners on protected branches.

### Approval Settings

* The author of the MR can't approve their own MR.
  It needs to be reviewed and approved by another user to check the changes and
  suggest improvements.
* Approval requires user re-authentication (password or SAML) to enforce security.
* When a commit is added to the MR post-approval, all approvals are kept to avoid
  long-lasting MR approvals.
  Indeed, if an approval is required after a new commit, approvers must review
  the MR again and check the new changes. Based on the number of commits, approving
  an MR can take a long time. For that reason, approvals are not removed when a new
  commit is added.

{{% warning %}}
It is not recommended to add commits after the MR is approved.
It could introduce failures that can take a while to fix.
The only allowed commits are to fix typos and Squash "fixup" commits created
during the review.

We trust our users to follow our recommendations.
{{% /warning %}}

