---
title: Architecture Design Records
linkTitle: ADRs
weight: 50
description: >
    Log every technical decisions made about how to implement the solution.
#icon: fa-solid fa-handshake
---

In this section, we keep track of every _important_ technical decisions about
how the solution is built. The aim is to remember the rationale behind the
choices we made and to share the knowledge.

An ADR shall:

* be written for every choice that may have a significant impact on the product,
  the team or the users
* be written when multiple choices are available
* address a single topic
