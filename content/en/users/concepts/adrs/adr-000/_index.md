---
linkTitle: Template # {Short Title}
title: >
    [ADR-000] {Long Title}
description: >
  Template of ADR.
date: 1970-01-01   # Created_at

# Icon: uncomment ONE (depending on the current state)
# -------------------------------------------------
icon: fa-solid fa-person-digging # in progress
#icon: fa-solid fa-circle-xmark   # rejected
#icon: fa-solid fa-circle-check   # accepted
#icon: fa-solid fa-box-archive    # deprecated
#icon: fa-solid fa-rotate         # superseded

# /!\ Set to 'false' (or remove the line) to publish the ADRs
# -------------------------------------------------
draft: true
---

## Information

{{% titleblock %}}
<!-- markdownlint-disable MD032 MD007 -->
Status
: `proposed (in progress)`

Update
: yyyy-mm-dd

Creation
: yyyy-mm-dd

Authors
: * {Name}.

Deciders
: * (✅ / ❌) {Name}
  * (✅ / ❌) {Name}
  * (✅ / ❌) {Name}

Consulted
: * {Name}.

Informed
: * {Name}.

<!-- markdownlint-enable MD032 MD007 -->
{{% /titleblock %}}

{{% info %}}
Other valid statuses are:
`proposed (in progress) | rejected | accepted | deprecated | superseded by {adr_id}`
{{% /info %}}

## Context and Problem Statement

{Summary of problem}.

## Decision Drivers

{TODO}.

## Considered Options

* {Title & summary (one line)}.

## Decision Outcome

The option "{title}" has been chosen because: {description}

### Positive Consequences

{TODO if any}.

### Negative Consequences

{TODO if any}.

## Validation

{How to test the conformance with the ADR}.

## Pros and Cons of the Options

### Option 1: {Title}

#### Summary

{Short description}.

{{< cardpane >}}
  {{% card header="Pros (&#x1F44D;)" %}}

* item

  {{% /card %}}
  {{% card header="Cons (&#x1F44E;)" %}}

* item

  {{% /card %}}
{{< /cardpane >}}

#### Description

{Details}.

## More Information

{TODO if any}.
