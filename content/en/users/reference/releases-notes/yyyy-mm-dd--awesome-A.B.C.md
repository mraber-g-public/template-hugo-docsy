---
title: Release A.B.C
linkTitle: Awesome A.B.C
slug: awesome-A.B.C
date: 1970-01-01
description: Awesome A.B.x security patch / Awesome A.B.0 minor update
draft: true
---

{{< gitlab-release "vA.B.C" >}} {{< upgrade-path >}}

## Key improvements

- item

## Bug fixes & CVEs

- {{< cve-link CVE-YYYY-nnnn >}}
- other bugfix

## Important notes

{{< warning >}}
You should always refer to the previous release notes:

- Initial release: [A.B.0]
- Patches: [A.B.1], [A.B.2], [A.B.3]

[A.B.0]: {{< relref "yyyy-mm-dd--awesome-A.B.C.md" >}}
[A.B.1]: {{< relref "yyyy-mm-dd--awesome-A.B.C.md" >}}
[A.B.2]: {{< relref "yyyy-mm-dd--awesome-A.B.C.md" >}}
[A.B.3]: {{< relref "yyyy-mm-dd--awesome-A.B.C.md" >}}

{{< /warning >}}

### Global

{some remarks}

### Component 1

{some remarks}

## Changes

TODO

## Deprecations

TODO

## Details

TODO
