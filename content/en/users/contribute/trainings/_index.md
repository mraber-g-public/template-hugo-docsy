---
title: Trainings
linkTitle: Trainings
weight: 42
description: Various presentations (slideshows) about how to contribute.
icon: fa-regular fa-person-chalkboard
---
