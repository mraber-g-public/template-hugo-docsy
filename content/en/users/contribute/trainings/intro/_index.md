---
title: Introduction
description: >
    A short introduction about the solution: what is it, the tools we use, etc.
# /!\ Do NOT display the content of the slideshow in the ToC.
#     (it's not designed for that)
toc_hide: true
outputs: ["Reveal"]
cascade:
  params:
    type: "reveal"
reveal_hugo:
  theme: moon
---
