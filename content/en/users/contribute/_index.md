---
title: Contributing Guide
linkTitle: Contribute
weight: 60
description: How to contribute to the solution.
icon: fa-solid fa-handshake-angle
---

The goal of this section is to help you contribute to the solution.
To do that, we will help you understand the structure, its components and the workflow.

There are various ways you can contribute:

- Submit a bug / a request
- Improve the documentation
- Develop a fix or a new feature
