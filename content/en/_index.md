---
title: Template Hugo Docsy
---

<!-- markdownlint-disable MD033 MD046 -->
{{< blocks/cover title="Template Hugo Docsy" height="min" >}}

A sample Docsy website to document software solutions.

{{< /blocks/cover >}}

{{% blocks/lead color="light" height="auto" %}}

<i class="fa-regular fa-industry h1"></i>

    Our new solution is the best way to be productive and
    create a better user experience.

{{% /blocks/lead %}}

{{< blocks/section color="dark" type="row" >}}

{{% blocks/feature icon="fa-book" title="Awesome" %}}

<div>
  <a class="btn btn-lg btn-primary me-3 mb-4"
     href="users/">
    User Manual
  </a>
</div>

{{% /blocks/feature %}}

{{% blocks/feature icon="fa-brands fa-github"
                   title="Contributions welcome!"
                   url="users/contribute" %}}
<div>
  <a class="btn btn-lg btn-secondary me-3 mb-4"
     href="https://mraber-g-public.gitlab.io/template-hugo-docsy/">
    Documentation <i class="fab fa-github ms-2 "></i>
  </a>
  <a class="btn btn-lg btn-secondary me-3 mb-4"
     href="https://mraber-g-public.gitlab.io/template-hugo-docsy/">
    Source Code <i class="fab fa-github ms-2 "></i>
  </a>
</div>

{{% /blocks/feature %}}

{{% blocks/feature icon="fa-people-group" title="The Team" %}}

<a class="btn btn-lg btn-primary me-3 mb-4" href="team/">
  Workspace
</a>

{{% /blocks/feature %}}

{{< /blocks/section >}}

<!-- markdownlint-enable MD033 MD046 -->
